package array;
import java.util.Scanner;

public class appartment {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("enter total no of floors");
        int floors = sc1.nextInt();
        System.out.println("enter total no of flat on each floor");
        int flat = sc1.nextInt();
        int[][] data = new int[floors][flat];
        System.out.println("enter " + (floors * flat) + "flat");

        for (int a = 0; a < floors; a++) {
            for (int b = 0; b < flat; b++) {
                data[a][b] = sc1.nextInt();

            }
        }
        System.out.println("===========================");
        for (int a = 0; a < floors; a++) {
            System.out.println("floor no" + (a + 1));

            for (int b = 0; b < flat; b++) {
                System.out.print("flat no:" + data[a][b] + "\t");
            }
            System.out.println();
            System.out.println("==============================");
        }
    }
}
