package string;

public class stringDemo {
    public static void main(String[] args) {
        String s="software Developer";
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf('e'));
        System.out.println(s.lastIndexOf('a'));
        System.out.println(s.startsWith("soft"));
        System.out.println(s.endsWith("er"));
        System.out.println(s.substring(9));
        System.out.println(s.substring(0,9));
        System.out.println(s.contains("elo"));

        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
    }
}
