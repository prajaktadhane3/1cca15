package test;

public class student {
    int studentid;
    String studentname;
    double studentpercentage;

    student( int id,String name,double percent)
    {
        studentid=id;
        studentname=name;
        studentpercentage=percent;
    }
    void display()
    {
        System.out.println(studentid +"\t"+studentname+"\t"+studentpercentage);
    }

}
 class mainapp2
 {
     public static void main(String[] args) {
         student s1=new student(101,"sujit",85.25);
         s1.display();

         student s2=new student(102,"vivek",63.44);
         s2.display();
     }
 }
