package test;

public class blockdemo4 {

    static
    {
        System.out.println("static block");
    }

    public static void main(String[] args)
    {
        System.out.println("main method");
        blockdemo4 b1=new blockdemo4();
        blockdemo4 b2=new blockdemo4();

    }
    {
        System.out.println("non static block");
    }
}
