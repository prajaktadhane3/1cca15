package abstraction;

public class loanaccount implements account{
double loanAmount;
public loanaccount(double loanAmount)
{
    this.loanAmount=loanAmount;
    System.out.println("Loan Account created");
}
    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"RS debited froam loan account");
    }

    @Override
    public void withdraw(double amt) {
       loanAmount+=amt;
        System.out.println(amt+"RS credited to your account");
    }

    @Override
    public void checkBalance() {
        System.out.println("active loan amount"+loanAmount);
    }
}
