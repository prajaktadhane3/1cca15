package abstraction;

public class savingAccount implements account{
    double accountBalance;
    public savingAccount(double accountBalance)
    {
        this.accountBalance=accountBalance;
        System.out.println("saving Account created");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"RS credited to your account");
    }

    @Override
    public void withdraw(double amt) {
if (amt<=accountBalance)
{
    accountBalance-=amt;
    System.out.println(amt+"RS debited from your account");
}
else {
    System.out.println("Insufficient account balance");
}
    }

    @Override
    public void checkBalance() {
        System.out.println("active balance"+accountBalance);
    }
}
