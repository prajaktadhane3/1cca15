package abstraction;

import java.util.Scanner;

public class mainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select account type");
        System.out.println("1:saving\n2:Loan");
        int accType=sc1.nextInt();
        System.out.println("enter account opening balance");
        double balance= sc1.nextInt();

        AccountFactory factory=new AccountFactory();
        account accRef=factory.createAccount(accType,balance);
        boolean status=true;
        while(status)
        {
            System.out.println("select mode of transaction");
            System.out.println("1:Deposit\n2:Withdraw\n3:check balance\n4:exit");
            int choice= sc1.nextInt();
            if(choice==1)
            {
                System.out.println("enter amount");
                double amt=sc1.nextInt();
                accRef.deposit(amt);
            } else if (choice==2) {
                System.out.println("enter amount");
                double amt=sc1.nextInt();
                accRef.withdraw(amt);

            } else if (choice==3) {
                accRef.checkBalance();

            }else {
                status=false;
            }
        }
    }
}
