package abstraction;

public interface account {
    void deposit(double amt);
    void withdraw(double amt);
    void checkBalance();
}
