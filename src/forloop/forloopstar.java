package forloop;

public class forloopstar {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        for (int row=1;row<=line;row++)
        {
            for (int col=1;col<=star;col++)
            {
                System.out.print("  *");
            }
            System.out.println();
            star++;
        }
    }
}
