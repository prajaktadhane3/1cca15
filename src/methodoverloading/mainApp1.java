package methodoverloading;

import java.util.Scanner;

public class mainApp1 {
    public static void main(String[] args)
    {
        m1 m=new m1();
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter the search type");
        System.out.println("1.by name");
        System.out.println("2.by contact");
        int choice=sc1.nextInt();
        if(choice==1)
        {
            System.out.println("enter name");
            String name=sc1.next();
            m.search(name);

        } else if (choice==2) {
            System.out.println("enter contact no");
            int contact=sc1.nextInt();
            m.search(contact);

        }
        else {
            System.out.println("invalid choice");
        }


    }
}
